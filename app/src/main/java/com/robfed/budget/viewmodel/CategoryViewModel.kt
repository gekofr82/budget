package com.robfed.budget.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.robfed.budget.database.AppDatabase
import com.robfed.budget.model.Category
import com.robfed.budget.model.CategoryOperations
import com.robfed.budget.repository.CategoryRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class CategoryViewModel (application: Application) : AndroidViewModel(application) {

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository: CategoryRepo
    val allCategories : LiveData<List<Category>>
    val listCategoryOperations : LiveData<List<CategoryOperations>>

    init {
        val dao = AppDatabase.getAppDataBase(application, scope)!!.categoryDao()
        repository = CategoryRepo(dao)
        allCategories = repository.allCategories
        listCategoryOperations = repository.allCategorieOperations
    }

    fun insert(category: Category) = scope.launch(Dispatchers.IO) {
        repository.insert(category)
    }

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }
}