package com.robfed.budget.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.robfed.budget.database.AppDatabase
import com.robfed.budget.model.Operation
import com.robfed.budget.model.OperationCategory
import com.robfed.budget.repository.OperationRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class OperationViewModel (application: Application) : AndroidViewModel(application) {

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository: OperationRepo
    val allOperations : LiveData<List<Operation>>

    init {
        val operationDao = AppDatabase.getAppDataBase(application, scope)!!.operationDao()
        repository = OperationRepo(operationDao)
        allOperations = repository.allOperation
    }

    fun insert(operation : Operation) = scope.launch(Dispatchers.IO) {
        repository.insert(operation)
    }

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

    fun getOperation(month : Int , year : Int , limit : Int) : LiveData<List<OperationCategory>> {
        return repository.getOperation(month, year, limit)
    }

}