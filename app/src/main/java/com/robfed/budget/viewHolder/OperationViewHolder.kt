package com.robfed.budget.viewHolder

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.robfed.budget.model.Category
import com.robfed.budget.model.OperationCategory
import kotlinx.android.synthetic.main.operation_item.view.*
import java.text.SimpleDateFormat

class OperationViewHolder(itemView: View) : ViewHolder(itemView) {
    val valuta : String = "€"

    fun bindView(operation: OperationCategory, cx : Context) {
        var category : Category = operation.category.get(0)
        itemView.icon_category_op.setImageDrawable(cx.getDrawable(category.icon))
        itemView.name_operation.text = operation.memo
        itemView.date_operation.text = getDateString(operation.date)
        var sign : String = if (operation.revenue) "+" else "-"
        itemView.price_operation.text = "$sign ${operation.price} $valuta"
        itemView.color_category.setColorFilter(ContextCompat.getColor(cx, category.color))
    }


    fun getDateString(milliSeconds : Long) : String {
        var formatter = SimpleDateFormat("dd MMMM")
        return formatter.format(milliSeconds).toString()
    }
}