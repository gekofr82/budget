package com.robfed.budget.viewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.robfed.budget.model.Category
import kotlinx.android.synthetic.main.category_item.view.*

class CategoryViewHolder(itemView: View) : ViewHolder(itemView) {

    fun bindView(category: Category) {
        itemView.color_category.setBackgroundResource(category.color)
        itemView.icon_category.setImageResource(category.icon)
        itemView.name_category.text = category.nameCategory
    }

}