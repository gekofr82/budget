package com.robfed.budget.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.robfed.budget.model.Category
import com.robfed.budget.model.CategoryOperations

@Dao
interface CategoryDao {

    @Insert
    fun insert(category: Category)

    @Update
    fun update(category: Category)

    @Delete
    fun delete(category: Category)

    @Query("DELETE FROM categoryTable")
    fun deleteAll()

    @Query("SELECT * from categoryTable ORDER BY name_category ASC")
    fun getAllCategories(): LiveData<List<Category>>

    @Query("SELECT id, name_category, icon, color    FROM categoryTable")
    fun getCategoryOperations() : LiveData<List<CategoryOperations>>
}