package com.robfed.budget.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.robfed.budget.model.Operation
import com.robfed.budget.model.OperationCategory

@Dao
interface OperationDao {

    @Insert
    fun insert(operation: Operation)

    @Update
    fun update(operation: Operation)

    @Delete
    fun delete(operation: Operation)

    @Query("DELETE FROM operationTable")
    fun deleteAll()

    @Query("SELECT * from operationTable ORDER BY year ASC")
    fun getAllOperations(): LiveData<List<Operation>>

    @Query("SELECT id, revenue, memo, date, price, year , month, day, id_category  FROM operationTable WHERE month = :month AND year = :year ORDER BY date DESC LIMIT :limit")
    fun getOperationsCategory(month : Int , year : Int , limit : Int) : LiveData<List<OperationCategory>>

    @Query("SELECT id, revenue, memo, date, price, year , month, day, id_category  FROM operationTable WHERE month = :month AND year = :year ORDER BY date DESC")
    fun getAllOperationsCategory(month : Int , year : Int) : LiveData<List<OperationCategory>>

}
