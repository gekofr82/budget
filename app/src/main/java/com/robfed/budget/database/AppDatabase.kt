package com.robfed.budget.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.robfed.budget.R
import com.robfed.budget.converter.DateTypeConverter
import com.robfed.budget.dao.CategoryDao
import com.robfed.budget.dao.OperationDao
import com.robfed.budget.database.AppDatabase.Companion.INSTANCE
import com.robfed.budget.model.Category
import com.robfed.budget.model.Operation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

@Database(entities = [Category::class, Operation::class], version = 1)
@TypeConverters(DateTypeConverter::class)
abstract class AppDatabase  : RoomDatabase(){
    abstract fun operationDao() : OperationDao
    abstract fun categoryDao() : CategoryDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context, scope : CoroutineScope): AppDatabase? {
            if (INSTANCE == null){
                synchronized(AppDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "budgetDB").addCallback(DatabaseCallback(scope)).build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}


private class DatabaseCallback(private val scope: CoroutineScope) : RoomDatabase.Callback() {
    override fun onOpen(db: SupportSQLiteDatabase) {
        super.onOpen(db)
        INSTANCE?.let { database ->
            scope.launch(Dispatchers.IO) {
                //populateDatabase(database.categoryDao() , database.operationDao())
            }
        }
    }

    override fun onCreate(db: SupportSQLiteDatabase) {
        super.onCreate(db)
        INSTANCE?.let { database ->
            scope.launch(Dispatchers.IO) {
                populateDatabase(database.categoryDao() , database.operationDao())
            }
        }
    }

    private fun populateDatabase(categoryDao: CategoryDao, opeationDao : OperationDao) {
        opeationDao.deleteAll()
        categoryDao.deleteAll()

        var general = Category(id= 1, nameCategory = "General", icon = R.drawable.ic_monetization, color = R.color.light_green)
        var pets = Category(id= 2, nameCategory = "Pets", icon = R.drawable.ic_pets, color = R.color.blue_grey)
        var resto = Category(id= 3, nameCategory = "Resto", icon = R.drawable.ic_cafe, color = R.color.yellow)
        var salary = Category(id= 4, nameCategory = "Salary", icon = R.drawable.ic_payment, color = R.color.amber)
        var transports = Category(id= 5, nameCategory = "Transports", icon = R.drawable.ic_bus, color = R.color.orange)
        var medoc = Category(id= 6, nameCategory = "Medoc", icon = R.drawable.ic_medoc, color = R.color.cyan)
        var fitness = Category(id= 7, nameCategory = "Sport", icon = R.drawable.ic_fitness, color = R.color.deep_purple)
        var course = Category(id= 8, nameCategory = "Spesa", icon = R.drawable.ic_grocery, color = R.color.teal)

        categoryDao.insert(general)
        categoryDao.insert(pets)
        categoryDao.insert(resto)
        categoryDao.insert(salary)
        categoryDao.insert(transports)
        categoryDao.insert(medoc)
        categoryDao.insert(fitness)
        categoryDao.insert(course)

        var calendarToday : Calendar = Calendar.getInstance()
        var calendarYesterday : Calendar = Calendar.getInstance()
        calendarYesterday.set(2019, 3, 2, 19, 45)
        calendarYesterday.timeInMillis
        var operation0 = Operation(null, true, "Stipendio", calendarToday.timeInMillis, 2500.00 , 2019, 4, 1,  4)
        var operation1 = Operation(null, false, "Cibo Fuffi", System.currentTimeMillis(), 50.00 , 2019, 4, 1,  2)
        var operation2 = Operation(null, false, "Cena Famiglia", System.currentTimeMillis(), 100.00 , 2019, 4, 1,  3)
        var operation3 = Operation(null, false, "Cena Colleghi", System.currentTimeMillis(), 25.00 , 2019, 4, 1,  3)
        var operation4 = Operation(null, false, "Tramezzino", System.currentTimeMillis(), 5.00 , 2019, 4, 1,  3)
        var operation5 = Operation(null, false, "Abbonamento RATP", System.currentTimeMillis(), 72.00 , 2019, 4, 1,  5)
        var operation6 = Operation(null, false, "Medicine", System.currentTimeMillis(), 15.00 , 2019, 4, 1,  6)
        var operation7 = Operation(null, false, "Palestra Lucille", System.currentTimeMillis(), 34.00 , 2019, 4, 1,  6)
        var operation8 = Operation(null, false, "Spesa", System.currentTimeMillis(), 70.00 , 2019, 4, 1,  8)
        var operation9 = Operation(null, false, "Spesa", calendarYesterday.timeInMillis, 25.00 , 2019, 4, 1,  8)

        opeationDao.insert(operation0)
        opeationDao.insert(operation1)
        opeationDao.insert(operation2)
        opeationDao.insert(operation3)
        opeationDao.insert(operation4)
        opeationDao.insert(operation5)
        opeationDao.insert(operation6)
        opeationDao.insert(operation7)
        opeationDao.insert(operation8)
        opeationDao.insert(operation9)
    }
}