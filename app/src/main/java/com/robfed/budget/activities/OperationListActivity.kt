package com.robfed.budget.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.robfed.budget.R
import com.robfed.budget.adapters.OperationAdapter
import com.robfed.budget.viewmodel.OperationViewModel
import kotlinx.android.synthetic.main.content_operation_list.*
import java.util.*

class OperationListActivity : AppCompatActivity() {

    private lateinit var model: OperationViewModel
    private lateinit var adapter : OperationAdapter
    private var selectedDate : Calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_operation_list)
        actionBar?.setDisplayHomeAsUpEnabled(true)


        list_transactions.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        adapter = OperationAdapter()
        list_transactions.adapter = adapter
        model = ViewModelProviders.of(this).get(OperationViewModel::class.java)
        loadLastOperation()

        selectorCalendar.buttonDownTapped = {
            selectedDate = selectorCalendar.getDate()
            loadLastOperation()
        }

        selectorCalendar.buttonUpTapped = {
            selectedDate = selectorCalendar.getDate()
            loadLastOperation()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out)
        return true
    }

    fun loadLastOperation(){
        var month : Int = selectedDate.get(Calendar.MONTH)
        var year : Int = selectedDate.get(Calendar.YEAR)
        model.getOperation(month+1, year, 0).observe(this, Observer { items ->
            items?.let {
                adapter.setList(it)
            }
        })
    }
}
