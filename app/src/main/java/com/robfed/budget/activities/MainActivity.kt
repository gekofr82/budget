package com.robfed.budget.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.robfed.budget.R
import com.robfed.budget.adapters.OperationAdapter
import com.robfed.budget.adapters.ViewPagerAdapter
import com.robfed.budget.viewmodel.OperationViewModel
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator
import kotlinx.android.synthetic.main.activity_category_list.*
import kotlinx.android.synthetic.main.content_activity.*
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var model: OperationViewModel
    private lateinit var adapter : OperationAdapter
    private var selectedDate : Calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val viewPager = findViewById<ViewPager>(R.id.view_pager)
        val dotsIndicator = findViewById<DotsIndicator>(R.id.dots_indicator)
        if (viewPager != null) {
            val adapter = ViewPagerAdapter(supportFragmentManager)
            viewPager.adapter = adapter
        }
        dotsIndicator.setViewPager(viewPager)

        last_transactions.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        adapter = OperationAdapter()
        last_transactions.adapter = adapter
        model = ViewModelProviders.of(this).get(OperationViewModel::class.java)
        loadLastOperation()

        button_see_more.setOnClickListener {
            var intent = Intent(this, OperationListActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out)
        }

        selector_.buttonDownTapped = {
            selectedDate = selector_.getDate()
            loadLastOperation()
        }

        selector_.buttonUpTapped = {
            selectedDate = selector_.getDate()
            loadLastOperation()
        }
    }

    private fun getMonthString() : String {
        var formatter = SimpleDateFormat("MMMM yyyy")
        return formatter.format(selectedDate.timeInMillis).toString()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.home_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_category -> {
            var intent = Intent(this, CategoryListActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out)
            true
        }
        else -> {

            super.onOptionsItemSelected(item)
        }
    }

    fun loadLastOperation(){
        var month : Int = selectedDate.get(Calendar.MONTH)
        var year : Int = selectedDate.get(Calendar.YEAR)
        model.getOperation(month+1, year, 10).observe(this, Observer { items ->
            items?.let {
                adapter.setList(it)
            }
        })
    }

    fun addOperation(view : View) {
        var intent = Intent(this, OperationActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.activity_in, R.anim.activity_out)
    }
}

