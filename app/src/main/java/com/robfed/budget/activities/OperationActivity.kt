package com.robfed.budget.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.FirebaseApp
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.robfed.budget.R
import com.robfed.budget.Utils
import com.robfed.budget.adapters.SpinnerCategoryAdapter
import com.robfed.budget.model.Category
import com.robfed.budget.model.Operation
import com.robfed.budget.viewmodel.CategoryViewModel
import com.robfed.budget.viewmodel.OperationViewModel
import kotlinx.android.synthetic.main.activity_operation.*
import kotlinx.android.synthetic.main.content_operation.*
import java.text.SimpleDateFormat
import java.util.*

class OperationActivity : AppCompatActivity() {
    val GALLERY_REQ_CODE: Int = 1909

    private lateinit var categoryViewModel: CategoryViewModel
    private lateinit var model: OperationViewModel

    val listCategory: List<Category> = ArrayList()
    private var selectedDate: Calendar = Calendar.getInstance()
    private var selectedType: Boolean = false
    private var category: Category? = null

    private var isModofica = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        FirebaseApp.initializeApp(this)

        //Spinner type operation
        val spinner: Spinner = findViewById(R.id.spinner_type_operation)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedType = position == 0
            }
        }
        ArrayAdapter.createFromResource(this, R.array.type_operation, android.R.layout.simple_spinner_item)
            .also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
            }

        //spinner_category
        val spinnerCategory: Spinner = findViewById(R.id.spinner_category)
        var spinnerAdapter = SpinnerCategoryAdapter(this, listCategory)
        spinnerCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                category = spinnerAdapter.getItem(position)
            }
        }
        spinnerCategory.adapter = spinnerAdapter

        //ViewModel
        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        categoryViewModel.allCategories.observe(this, Observer { categories ->
            categories?.let {
                spinnerAdapter.reloadList(it)
                category = it.get(0)
            }
        })
        spinner.setSelection(0)

        //Date
        selected_date.text = getMonthString()
        date_operation.setOnClickListener(View.OnClickListener { view ->
            val year = selectedDate.get(Calendar.YEAR)
            val month = selectedDate.get(Calendar.MONTH)
            val day = selectedDate.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                selectedDate.set(Calendar.YEAR, year)
                selectedDate.set(Calendar.MONTH, monthOfYear)
                selectedDate.set(Calendar.DATE, dayOfMonth)
                selected_date.text = getMonthString()

            }, year, month, day)
            dpd.show()
        })

        //Button Send Operation
        button_add_operation.setOnClickListener(View.OnClickListener { v ->
            if (checkOperation()) {
                sendInsertOperation()
            }
        })

        //Button Scan Ticket CB
        scan_ticket.setOnClickListener(View.OnClickListener {
            var intentGallery: Intent = Intent(this, CameraActivity::class.java)
            startActivityForResult(intentGallery, Utils.CAMERA_REQ_CODE)
        })

        gallery_ticket.setOnClickListener(View.OnClickListener {
            var intentGallery: Intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            startActivityForResult(intentGallery, GALLERY_REQ_CODE)
        })

        model = ViewModelProviders.of(this).get(OperationViewModel::class.java)
    }

    private fun sendInsertOperation() {
        var memo: String = input_memo.text.toString()
        var revenue: Boolean = selectedType
        var date: Long = selectedDate.timeInMillis
        var price: Double = input_price.text.toString().toDouble()
        var year: Int = selectedDate.get(Calendar.YEAR)
        var month: Int = selectedDate.get(Calendar.MONTH)
        var day: Int = selectedDate.get(Calendar.DATE)
        var idCategory: Int = category?.id!!
        var operation: Operation = Operation(null, revenue, memo, date, price, year, month + 1, day, idCategory)
        model.insert(operation)
        Toast.makeText(this, "Operazione " + operation.memo + " inserita con successo", Toast.LENGTH_SHORT).show()
        onSupportNavigateUp()
    }

    private fun checkOperation(): Boolean {
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out)
        return true
    }

    private fun getMonthString(): String {
        var formatter = SimpleDateFormat("dd MMMM yyyy")
        return formatter.format(selectedDate.timeInMillis).toString()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            lateinit var bitmap: Bitmap
            if (requestCode == GALLERY_REQ_CODE) {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data?.data)

            } else if (requestCode == Utils.CAMERA_REQ_CODE) {
                var absolutePath = data!!.getStringExtra(Utils.CAMERA_DATA)
                if (!absolutePath.isBlank()) {
                    bitmap = BitmapFactory.decodeFile(absolutePath)
                }
            }
            try {
                Log.d("FIGA", "return operation")
                val image = FirebaseVisionImage.fromBitmap(bitmap)
                val detector = FirebaseVision.getInstance().onDeviceTextRecognizer
                var result = detector.processImage(image).addOnSuccessListener { result ->
                    var operation = Utils.getOperationFromPhoto(result)
                    preselectOperation(operation)
                }
            } catch (e: java.lang.Exception) {
                //TODO ERROR
            }
        }
    }

    private fun preselectOperation(operation: Operation) {
        Log.d("FIGA", "return data")
        input_price.setText(operation.price.toString())
        selectedDate.timeInMillis = operation.date
        selected_date.text = getMonthString()
    }


}
