package com.robfed.budget.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.robfed.budget.R
import com.robfed.budget.Utils
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.File
import java.io.FileOutputStream


class CameraActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
    }

    override fun onStart() {
        camera_kit.onStart()
        super.onStart()
    }

    override fun onResume() {
        camera_kit.onResume()
        super.onResume()

    }

    override fun onPause() {
        camera_kit.onPause()
        super.onPause()
    }

    override fun onStop() {
        camera_kit.onStop()
        super.onStop()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        camera_kit.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun tekePicture(view: View) {
        camera_kit.captureImage { cameraKitView, bytes ->
            val savedPhoto = File(Environment.getExternalStorageDirectory(), "budgetCB.jpg")
            try {
                val outputStream = FileOutputStream(savedPhoto.getPath())
                outputStream.write(bytes)
                outputStream.close()
            } catch (e: java.io.IOException) {
                e.printStackTrace()
            }
            var intentReturn: Intent = Intent()
            intentReturn.putExtra(Utils.CAMERA_DATA, savedPhoto.absolutePath)
            setResult(Activity.RESULT_OK, intentReturn)
            finish()
        }
    }

    override fun onBackPressed() {
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out)
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }
}



