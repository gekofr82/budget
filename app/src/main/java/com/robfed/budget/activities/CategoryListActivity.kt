package com.robfed.budget.activities

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.robfed.budget.R
import com.robfed.budget.adapters.CategoryAdapter
import com.robfed.budget.viewmodel.CategoryViewModel
import kotlinx.android.synthetic.main.activity_category_list.*
import kotlinx.android.synthetic.main.content_category_list.*

class CategoryListActivity : AppCompatActivity() {

    private lateinit var categoryViewModel: CategoryViewModel
    private lateinit var adapter : CategoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_list)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerViewCategories.layoutManager = GridLayoutManager(this, 3) as RecyclerView.LayoutManager?

        //This will for default android divider
        //recyclerViewMovies.addItemDecoration(GridItemDecoration(10, 2))
        adapter = CategoryAdapter()
        recyclerViewCategories.adapter = adapter

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        categoryViewModel.allCategories.observe(this, Observer { categories ->
            categories?.let {
                adapter.setCategoryList(it)
            }
        })

        categoryViewModel.listCategoryOperations.observe(this, Observer { categories ->
            categories?.let {
                for (item in it) {
                    Log.d("figaaaa", item.toString())
                }
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out)
        return true
    }

}
