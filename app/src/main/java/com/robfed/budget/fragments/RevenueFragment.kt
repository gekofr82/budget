package com.robfed.budget.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.robfed.budget.R
import com.robfed.budget.model.CategoryOperations
import com.robfed.budget.viewmodel.CategoryViewModel
import kotlinx.android.synthetic.main.fragment_graph.*
import kotlinx.android.synthetic.main.fragment_graph.view.*

class RevenueFragment : Fragment() {

    private lateinit var model: CategoryViewModel
    private var listCategoryOperation : List<CategoryOperations> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_graph, container, false)
        view.title_graph.text = "Revenue"
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            model = ViewModelProviders.of(it).get(CategoryViewModel::class.java)
            model.listCategoryOperations.observe(this, Observer { categories ->
                categories?.let {
                    listCategoryOperation = it
                    setUpPieChartData()
                }
            })
        }
    }

    private fun setUpPieChartData() {
        val yVals = ArrayList<PieEntry>()
        yVals.add(PieEntry(30f))
        yVals.add(PieEntry(2f))
        yVals.add(PieEntry(4f))
        yVals.add(PieEntry(22f))
        yVals.add(PieEntry(12.5f))

        val dataSet = PieDataSet(yVals, "")
        dataSet.valueTextSize=0f
        val colors = java.util.ArrayList<Int>()
        colors.add(resources.getColor(R.color.pastel_blue))
        colors.add(resources.getColor(R.color.pastel_green))
        colors.add(resources.getColor(R.color.pastel_orange))
        colors.add(resources.getColor(R.color.pastel_rose))
        colors.add(resources.getColor(R.color.pastel_yellow))
        dataSet.colors = colors

        var l = graph.legend as Legend
        // set custom labels and colors
        val legend = java.util.ArrayList<LegendEntry>()
        legend.add(
            LegendEntry("Categoria 1",
                Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_green))
        )
        legend.add(
            LegendEntry("Categoria 2",
                Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_blue))
        )
        legend.add(
            LegendEntry("Categoria 3",
                Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_orange))
        )
        legend.add(
            LegendEntry("Categoria 4",
                Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_rose))
        )
        legend.add(
            LegendEntry("Categoria 5",
                Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_yellow))
        )
        legend.add(
            LegendEntry("Categoria 5",
                Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_yellow))
        )
        legend.add(
            LegendEntry("Categoria 5",
                Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_yellow))
        )
        legend.add(
            LegendEntry("Categoria 5",
                Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_yellow))
        )
        l.setCustom(legend)

        val data = PieData(dataSet)
        graph.data = data
        graph.centerTextRadiusPercent = 0f
        graph.isDrawHoleEnabled = true
        graph.legend.isEnabled = true
        graph.description.isEnabled = false
        graph.transparentCircleRadius = 50f
        graph.setHoleColor(Color.WHITE)
        graph.isRotationEnabled = false
    }
}