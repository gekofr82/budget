package com.robfed.budget.fragments

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.robfed.budget.R
import com.robfed.budget.Utils
import com.robfed.budget.model.Operation
import com.robfed.budget.viewmodel.OperationViewModel
import kotlinx.android.synthetic.main.fragment_graph.*
import kotlinx.android.synthetic.main.fragment_graph.view.*

class OverviewFragment : Fragment() {

    private lateinit var model: OperationViewModel
    private var list : List<Operation> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_graph, container, false)
        view.title_graph.text = "Overview"
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            model = ViewModelProviders.of(it).get(OperationViewModel::class.java)
            model.allOperations.observe(this, Observer { items ->
                items?.let {
                    list = it
                    setUpPieChartData()

                }
            })
        }
    }


    @SuppressLint("ResourceAsColor")
    private fun setUpPieChartData() {
        var depense = Utils.getTotaleDepense(list)
        var revenue = Utils.getTotaleRevenue(list)

        val yVals = ArrayList<PieEntry>()
        yVals.add(PieEntry(depense.toFloat(), activity!!.getDrawable(R.drawable.ic_payment)))
        yVals.add(PieEntry(revenue.toFloat(), activity!!.getDrawable(R.drawable.ic_monetization)))

        val dataSet = PieDataSet(yVals, "")
        dataSet.valueTextSize=0f

        val colors = java.util.ArrayList<Int>()
        colors.add(resources.getColor(R.color.pastel_rose))
        colors.add(resources.getColor(R.color.pastel_green))
        dataSet.colors = colors

        var l = graph.legend as Legend
        // set custom labels and colors
        val legend = java.util.ArrayList<LegendEntry>()
        legend.add(LegendEntry("Total Depense",Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_rose)))
        legend.add(LegendEntry("Total Revenue",Legend.LegendForm.CIRCLE, 5f,5f, null, resources.getColor(R.color.pastel_green)))
        l.setCustom(legend)

        val data = PieData(dataSet)
        graph.centerTextRadiusPercent = 0f
        graph.isDrawHoleEnabled = true
        graph.legend.isEnabled = true
        graph.description.isEnabled = false
        graph.transparentCircleRadius = 50f
        graph.setHoleColor(Color.WHITE)
        graph.isRotationEnabled = false
        graph.data = data
        graph.notifyDataSetChanged()
        graph.invalidate()
    }
}