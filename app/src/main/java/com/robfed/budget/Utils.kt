package com.robfed.budget

import android.util.Log
import com.google.firebase.ml.vision.text.FirebaseVisionText
import com.robfed.budget.model.Operation
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    companion object {
        var CAMERA_REQ_CODE: Int = 1802
        var CAMERA_DATA: String = "CAMERA_DATA"

        fun getTotaleRevenue(allOperations: List<Operation>): Double {
            var totale: Double = 0.0
            for (item in allOperations) {
                if (item.revenue) {
                    totale += item.price
                }
            }
            return totale
        }

        fun getTotaleDepense(allOperations: List<Operation>): Double {
            var totale: Double = 0.0
            for (item in allOperations) {
                if (!item.revenue) {
                    totale += item.price
                }
            }
            return totale
        }

        fun getOperationFromPhoto(result: FirebaseVisionText): Operation {
            //REGEX
            var number = "[A-Za-a:]+".toRegex()
            var eur = "EUR".toRegex()
            var date = """\d{2}\/\d{2}\/\d{2}""".toRegex() //OK

            //VAR
            var impDouble = 0.0
            var dateCalendar = Calendar.getInstance()

            //
            for (block in result.textBlocks) {
                for (line in block.lines) {
                    //Log.d("FIGA", "text:" + line.text.toString())
                    if (line.text.contains(eur)) {
                        var importoString = line.text.replace(number, "")
                        impDouble = getImporto(importoString)
                        Log.d("FIGA", "importo " + importoString + "//" + impDouble)
                    }
                    if (line.text.contains(date)) {
                        var dateString = date.find(line.text)?.value.toString()
                        dateCalendar = getDate(dateString)
                        Log.d("FIGA", "dateString <" + dateString + ">")
                    }
                }
            }

            return Operation(
                0,
                false,
                "",
                dateCalendar.timeInMillis,
                impDouble,
                dateCalendar.get(Calendar.YEAR),
                dateCalendar.get(Calendar.MONTH),
                dateCalendar.get(Calendar.DATE),
                1
            )
        }

        fun getDate(dateString: String): Calendar {
            val calendar = Calendar.getInstance()
            try {
                val sdf = SimpleDateFormat("dd/MM/yy")
                var date = sdf.parse(dateString)
                calendar.setTime(date);
            } catch (e: Exception) {
                Log.e("ERROR", e.message)
            }
            return calendar
        }

        fun getImporto(importoString: String): Double {
            var importo = 0.0
            try {
                importo = importoString.replace(",", ".").toDouble()
            } catch (e: Exception) {
                Log.e("ERROR", e.message)
            }
            return importo
        }
    }

}