package com.robfed.budget.model

import androidx.room.*

@Entity(tableName = "operationTable", foreignKeys = [ForeignKey(entity = Category::class, parentColumns = ["id"], childColumns = ["id_category"])])
data class Operation(@PrimaryKey(autoGenerate = true) var id: Int?,
                     @ColumnInfo(name = "revenue") var revenue: Boolean,
                     @ColumnInfo(name = "memo") var memo: String,
                     @ColumnInfo(name = "date") var date: Long,
                     @ColumnInfo(name = "price") var price: Double,
                     @ColumnInfo(name = "year") var year: Int,
                     @ColumnInfo(name = "month") var month: Int,
                     @ColumnInfo(name = "day") var day: Int,
                     @ColumnInfo(name = "id_category") var idCategory: Int

)