package com.robfed.budget.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "categoryTable")
data class Category(@PrimaryKey(autoGenerate = true) var id: Int?,
                    @ColumnInfo(name = "name_category") var nameCategory: String,
                    @ColumnInfo(name = "icon") var icon: Int,
                    @ColumnInfo(name = "color") var color: Int
)