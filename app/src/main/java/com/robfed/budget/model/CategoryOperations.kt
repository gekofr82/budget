package com.robfed.budget.model

import androidx.room.ColumnInfo
import androidx.room.Relation

class CategoryOperations {

    var id: Int = 0
    @ColumnInfo(name = "name_category")
    var nameCategory: String = ""
    var icon: Int = 0
    var color: Int = 0

    @Relation(parentColumn = "id", entityColumn = "id_category", entity = Operation::class)
    var operations: List<Operation> = ArrayList()
}
