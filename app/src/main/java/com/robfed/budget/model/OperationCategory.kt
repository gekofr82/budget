package com.robfed.budget.model

import androidx.room.*

class OperationCategory{
    @ColumnInfo(name = "id") var id: Int = 0
    @ColumnInfo(name = "revenue") var revenue: Boolean = true
    @ColumnInfo(name = "memo") var memo: String = ""
    @ColumnInfo(name = "date") var date: Long = 0
    @ColumnInfo(name = "price") var price: Double = 0.0
    @ColumnInfo(name = "year") var year: Int = 0
    @ColumnInfo(name = "month") var month: Int = 0
    @ColumnInfo(name = "day") var day: Int = 0
    @ColumnInfo(name = "id_category") var idCategory: Int = 0

    @Relation(parentColumn = "id_category", entityColumn = "id", entity = Category::class)
    var category: List<Category> = emptyList()
}