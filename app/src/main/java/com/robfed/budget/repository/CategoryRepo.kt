package com.robfed.budget.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.robfed.budget.dao.CategoryDao
import com.robfed.budget.model.Category
import com.robfed.budget.model.CategoryOperations

class CategoryRepo(private val categoryDao: CategoryDao) {

    val allCategories: LiveData<List<Category>> = categoryDao.getAllCategories()
    val allCategorieOperations: LiveData<List<CategoryOperations>> = categoryDao.getCategoryOperations()

    @WorkerThread
    suspend fun insert(category: Category) {
        categoryDao.insert(category)
    }

    @WorkerThread
    suspend fun delete(category: Category) {
        categoryDao.delete(category)
    }

    @WorkerThread
    suspend fun update(category: Category) {
        categoryDao.update(category)
    }
}