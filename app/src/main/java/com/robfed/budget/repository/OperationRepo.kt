package com.robfed.budget.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.robfed.budget.dao.OperationDao
import com.robfed.budget.model.Operation
import com.robfed.budget.model.OperationCategory

class OperationRepo(private val operationDao: OperationDao) {

    val allOperation: LiveData<List<Operation>> = operationDao.getAllOperations()

    @WorkerThread
    suspend fun insert(operation: Operation) {
        operationDao.insert(operation)
    }

    @WorkerThread
    suspend fun delete(operation: Operation) {
        operationDao.delete(operation)
    }

    @WorkerThread
    suspend fun update(operation: Operation) {
        operationDao.update(operation)
    }

    fun getOperation(month : Int , year : Int , limit : Int) : LiveData<List<OperationCategory>>{
        if (limit > 0) {
            return operationDao.getOperationsCategory(month, year, limit)
        } else {
            return operationDao.getAllOperationsCategory(month, year)
        }
    }
}