package com.robfed.budget.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.robfed.budget.R
import kotlinx.android.synthetic.main.view_calendar_component.view.*
import java.text.SimpleDateFormat
import java.util.*


class CalendarSelector @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {
    private var formatter = SimpleDateFormat("MMMM yyyy")
    private var selectedDate = Calendar.getInstance()
    var buttonDownTapped : (() -> Unit)? = null
    var buttonUpTapped : (() -> Unit)? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.view_calendar_component, this, true)

        attrs?.let {

        }

        loadDate()
        button_up.setOnClickListener{
            addSubMonth(true)
            buttonUpTapped?.invoke()
        }


        button_down.setOnClickListener{
            addSubMonth(false)
            buttonDownTapped?.invoke()
        }
    }

    fun setDate(calendar : Calendar) {
        selectedDate = calendar
        loadDate()
    }

    fun getDate() : Calendar {
        return selectedDate
    }

    private fun loadDate() {
        var dateString = formatter.format(selectedDate.timeInMillis).toString()
        date_calendar.setText(dateString)
    }

    private fun addSubMonth(isAdd : Boolean){
        selectedDate.add(Calendar.MONTH, if (isAdd) 1 else -1)
        loadDate()
    }


}

