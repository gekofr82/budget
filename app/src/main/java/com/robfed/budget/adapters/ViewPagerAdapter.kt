package com.robfed.budget.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.robfed.budget.fragments.DepenseFragment
import com.robfed.budget.fragments.OverviewFragment
import com.robfed.budget.fragments.RevenueFragment

class ViewPagerAdapter internal constructor(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val COUNT = 3

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment = OverviewFragment()
        when (position) {
            0 -> fragment = OverviewFragment()
            1 -> fragment = DepenseFragment()
            2 -> fragment = RevenueFragment()
        }
        return fragment
    }

    override fun getCount(): Int {
        return COUNT
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Tab " + (position + 1)
    }
}
