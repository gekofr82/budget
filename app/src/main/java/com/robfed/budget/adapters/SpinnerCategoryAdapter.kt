package com.robfed.budget.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.robfed.budget.R
import com.robfed.budget.model.Category

class SpinnerCategoryAdapter (val context: Context, var listCategory: List<Category>) : BaseAdapter() {

    val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_spinner_category, parent, false)
            vh = ItemRowHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }

        vh.name.text = listCategory.get(position).nameCategory
        vh.image.setImageResource(listCategory.get(position).icon)
        return view
    }

    override fun getItem(position: Int): Category? {
        return listCategory.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return listCategory.size
    }

    private class ItemRowHolder(row: View?) {

        val name: TextView
        val image: ImageView

        init {
            this.image = row?.findViewById(R.id.iconCategory) as ImageView
            this.name = row?.findViewById(R.id.nameCategory) as TextView
        }
    }

    fun reloadList(listItemsTxt: List<Category>) {
        this.listCategory = listItemsTxt
        notifyDataSetChanged()
    }
}