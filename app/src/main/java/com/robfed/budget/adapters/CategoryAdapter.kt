package com.robfed.budget.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.robfed.budget.R
import com.robfed.budget.model.Category
import com.robfed.budget.viewHolder.CategoryViewHolder

class CategoryAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var listOFCategory = listOf<Category>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CategoryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.category_item, parent, false))
    }

    override fun getItemCount(): Int = listOFCategory.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val categoryViewHolder = holder as CategoryViewHolder
        categoryViewHolder.bindView(category = listOFCategory.get(position))
    }

    fun setCategoryList(listCategory : List<Category>){
        this.listOFCategory = listCategory
        notifyDataSetChanged()
    }
}