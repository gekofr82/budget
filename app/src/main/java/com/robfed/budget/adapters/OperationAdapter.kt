package com.robfed.budget.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.robfed.budget.R
import com.robfed.budget.model.OperationCategory
import com.robfed.budget.viewHolder.OperationViewHolder

class OperationAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list = listOf<OperationCategory>()
    private var cx: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.cx = parent.context
        return OperationViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.operation_item, parent, false))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as OperationViewHolder
        viewHolder.bindView(operation = list.get(position),cx = this.cx!!)
    }

    fun setList(list: List<OperationCategory>) {
        this.list = list
        notifyDataSetChanged()
    }
}